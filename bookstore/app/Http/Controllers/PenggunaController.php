<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Pengguna;

class PenggunaController extends Controller
{
	public function awal()
	{
    	return "Hello ini Controller";	
	}

	public function tambah()
	{
		return $this->simpan();
	}

	public function simpan()
	{
		$pengguna = new pengguna();
		$pengguna->username = 'ashar';
		$pengguna->password = 'ashar';
		$pengguna->save();
		return "Data dengan username ($pengguna->username) telah disimpan";
	}
}
