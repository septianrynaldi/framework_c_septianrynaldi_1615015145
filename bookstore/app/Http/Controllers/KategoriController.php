<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    //Read
    public function awal(){
    	$kategori = Kategori::all();
    	return view('kategori.app', compact('kategori'));
    }

    public function tambah(){
    	return view('kategori.tambah');
    }
    //Create
    public function simpan(Request $input){
        //buat validasi
         $this->validate($input,[
            'deskripsi'=>'required',
        ]);
         
    	$kategori = new Kategori();
    	$kategori->deskripsi = $input->deskripsi;
    	$status = $kategori->save();
    	return redirect('kategori')->with(['status'=>$status]);
    }

    public function edit($id){
    	$kategori = Kategori::find($id);
    	return view('kategori.editkategori')->with(array('kategori'=>$kategori));
    }

    //Update
    public function update($id, Request $input){
        //buat validasi
         $this->validate($input,[
            'deskripsi'=>'required',
        ]);
         
    	$kategori = Kategori::find($id);
    	$kategori->deskripsi = $input->deskripsi;
    	$status = $kategori->save();
    	return redirect('kategori');
    }

    //Delete
    public function hapus($id){
    	$kategori = Kategori::find($id);
    	$kategori->delete();
    	return redirect('kategori');
    }
}
