<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\pembeli;
use App\Pengguna;
 
class PembeliController extends Controller
{
    public function awal()
    {   $pembeli = Pembeli::all();   
        return view('pembeli.app', compact('pembeli'));   
    }    

    public function tambah()
    {   
        return view("pembeli.tambah");  
    }    

    public function simpan(Request $input)
    {  
        $this->validate($input,array(
            'username'=>'required',
            'password'=>'required',

            'nama'=>'required',
            'notlp'=>'required',
            'email'=>'required',
            'alamat'=>'required',
        ));   
        
        $pengguna = new Pengguna();   
        $pengguna->username = $input->username;   
        $pengguna->password = $input->password;   
        $pengguna->level = "village";   
        $pengguna->save();   
           
        $pembeli = new Pembeli();   
        $pembeli->nama = $input->nama;   
        $pembeli->notlp = $input->notlp;   
        $pembeli->email = $input->email;   
        $pembeli->alamat = $input->alamat;   
        $pembeli->pengguna_id = $pengguna->id;   
        $status = $pembeli->save();   
        return redirect('pembeli');  
    }

    public function edit($id)
    {   
        $pembeli = Pembeli::find($id);   
        return view('pembeli.edit')->with(array('pembeli'=>$pembeli));
    } 
    
    public function update($id, Request $input)
    {     
        $this->validate($input,array(
            'username'=>'required',
            'password'=>'required',

            'nama'=>'required',
            'notlp'=>'required',
            'email'=>'required',
            'alamat'=>'required',
        ));
        $pembeli = Pembeli::find($id);   
        $pembeli->nama = $input->nama;   
        $pembeli->notlp = $input->notlp;   
        $pembeli->email = $input->email;   
        $pembeli->alamat = $input->alamat;      
        $pengguna = Pengguna::find($pembeli->pengguna_id);   
        $pengguna->username = $input->username;   
        $pengguna->password = $input->password;   
        $pengguna->level = "village";   
        $pengguna->save();      
        $status = $pembeli->save();   
        return redirect('pembeli')->with(['status'=>$status]);  
    }    
    public function hapus($id)
    {   
        $pembeli = Pembeli::find($id);   
        $pembeli->pengguna()->delete();   
        return redirect('pembeli');  
    }      
}
