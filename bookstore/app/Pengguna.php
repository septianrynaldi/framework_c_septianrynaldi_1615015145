<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    protected $table = 'pengguna';
    protected $fillable = ['username','password'];

    public function Pembeli()
    {
		return $this->hasOne(Pembeli::class);
	}
	public function Admin()
    {
		return $this->hasOne('App\Admin');
	}

}
