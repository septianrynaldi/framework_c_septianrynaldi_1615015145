<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku_Pembeli extends Model
{
    protected $table = 'buku_pembeli';
    protected $fillable = ['pembeli_id','buku_id'];

    public function Pembeli()
    {
    	return $this->belongsToMany(Pembeli::class);
    }
    public function Buku()
    {
    	return $this->belongsToMany(Buku::class);
    }
}
