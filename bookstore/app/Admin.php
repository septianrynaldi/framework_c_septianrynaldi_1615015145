<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    protected $table = 'admin';
    protected $fillable = ['nama','notelp','email','alamat'];
    
    public function Pengguna()
    {
    	return $this->belongsTo(Pengguna::class);
    }
}
