@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Kategori
		<div class="pull-right">
			Tambah Data <a href="{{ url('kategori/tambah')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Id</td>
					<td>Deskripsi</td>
					<td></td>
					<td></td>
				</tr>
				@foreach($kategori as $Kategori)
					
				<tr>
					<td >{{ $Kategori->id }}</td>
					<td >{{ $Kategori->deskripsi }}</td>
					<td ></td>
					<td ></td>
					<td >
					
					<a href="{{url('kategori/edit/'.$Kategori->id)}}"><img src="{{ asset('icon/edit.png') }}" height="20"></img>Edit</a>
					<a href="{{url('kategori/hapus/'.$Kategori->id)}}"><img src="{{ asset('icon/hapus.png') }}" height="20"></img>Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

