<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Laravel</title>
	<link rel="stylesheet" type="text/css" href="{{ asset('css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('font-awesome/css/font-awesome.min.css') }}">
</head>
<body>
	<nav class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="{{url('/')}}" class="navbar-brand">Laravel</a>
			</div>
			<div class="collapse navbar-collapse" id="app-navbar-collapse">
				<!-- Left Side Of Navbar -->
				<ul class="nav navbar-nav">
					&nbsp;
				</ul>

				<!-- Right Side Of Navbar -->
				<ul class="nav navbar-nav navbar-right">
					<!-- Authentication Links -->
					<li><a href="{{ url('penulis') }}">Data Penulis</a></li>
					<li><a href="{{ url('buku') }}">Data Buku</a></li>
					<li><a href="{{ url('pembeli') }}">Data pembeli</a></li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
							Other <span class="caret"></span>
						</a>

						<ul class="dropdown-menu">
							<li><a href="{{ url('kategori') }}">Data Kategori</a></li>
							<li><a href="{{ url('admin') }}">Data Admin</a></li>
						</ul>
					</li>
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('logout') }}">Logout</a></li>
					
				</ul>
			</div>				
		</div>
	</nav>

	<div class="container">
		@if(Session::has('informasi'))
		<div class="alert alert-info">
			<strong>Informasi:</strong>
			{{ Session::get('informasi') }}
		</div>
		@endif
		@yield('content')
	</div>

	<nav class="navbar navbar-default navbar-fixed-bottom">
		<footer class="container">
			<p align="center">Created by Aslab Framework 2018</p>
		</footer>
	</nav>
	<script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
</body>
