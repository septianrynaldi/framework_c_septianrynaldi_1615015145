@extends('master')
@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Edit Data Pembeli
				</div>
				<div class="panel-body">
					{!! Form::model($penulis,['url'=>'penulis/update/'.$penulis->id,'class'=>'form-horizontal']) !!}
						@include('penulis.form')

						<div style="width:100%;text-align:center;">
							<button class="btn btn-primary"><i class="fa fa-save"></i>
							Simpan</button>
							<input type="button" value="Reset" class="btn btn-danger" onClick="window.location.reload()"/>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

