@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Admin
		<!--<div class="pull-right">
			Tambah Data <a href="{{ url('tambah/admin')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>-->
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
					<td>Pengguna</td>
				</tr>
				@foreach($admin as $Admin)
					
				<tr>
					<td >{{ $Admin->nama }}</td>
					<td >{{ $Admin->notlp}}</td>
					<td >{{ $Admin->email }}</td>
					<td >{{ $Admin->alamat}}</td>
					<td >{{ $Admin->pengguna->username}}</td>
					<td >
					
					<a href="{{url('admin/edit/'.$Admin->id)}}"><img src="{{ asset('icon/edit.png') }}" height="20"></img></a>
					<a href="{{url('admin/hapus/'.$Admin->id)}}"><img src="{{ asset('icon/hapus.png') }}" height="20"></img></a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

