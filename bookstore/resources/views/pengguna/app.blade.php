@extends('master')
@section('content')
{{ $status or ' ' }}
<div class="panel panel-info">
	<div class="panel-heading">
		Data Penulis
		<div class="pull-right">
			Tambah Data <a href="{{ url('tambah/penulis')}}"><button class="btn btn-primary">Tambah</button></a>
		</div>
	</div>
	<div class="panel-body">
		<table class="table">
				<tr>
					<td>Nama</td>
					<td>No Telepon</td>
					<td>Email</td>
					<td>Alamat</td>
				</tr>
				@foreach($penulis as $Penulis)
					
				<tr>
					<td >{{ $Penulis->nama }}</td>
					<td >{{ $Penulis->notlp}}</td>
					<td >{{ $Penulis->email }}</td>
					<td >{{ $Penulis->alamat}}</td>
					<td >
					
					<a href="{{url('penulis/edit/'.$Penulis->id)}}"><img src="{{ asset('icon/edit.png') }}" height="20"></img>Edit</a>
					<a href="{{url('penulis/hapus/'.$Penulis->id)}}"><img src="{{ asset('icon/hapus.png') }}" height="20"></img>Hapus</a>
					</td>
				</tr>
				@endforeach
			</table>
	</div>
</div>
@endsection

