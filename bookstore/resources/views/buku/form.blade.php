<div class="form-group">
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
	@endif 
</div>
<div class="form-group">
	<label class="col-sm-2">Judul Buku</label>
	<div class="col-sm-9">
		{!! Form::text('judul',null,['class'=>'form-control','placeholder'=>"Judul Buku"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Penerbit</label>
	<div class="col-sm-9">
		{!! Form::text('penerbit',null,['class'=>'form-control','placeholder'=>"Penerbit"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Tanggal Buku</label>
	<div class="col-sm-9">
		{!! Form::text('tanggal',null,['class'=>'form-control','placeholder'=>"Tanggal Buku"]) !!}
	</div>
</div>
<div class="form-group">
	<label class="col-sm-2">Kategori id</label>
	<div class="col-sm-9">
		{!! Form::text('kategori_id',null,['class'=>'form-control','placeholder'=>"Kategori id"]) !!}
	</div>
</div>
